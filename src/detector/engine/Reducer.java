package detector.engine;

import utils.containers.MapperJobDone;

import java.util.Iterator;
import java.util.Map;

/**
 * Reducer is implemented to calculate the similarities between two files using the
 * formula: sim(di, dj) = sum(f(t,di) * f(t,dj)) [%]
 * di and dj - documents to be checked
 * f(t, di) - the frequency of word t in di file
 * V - word reunion between the two files
 *
 * f(t, d) = (specific_word_count(t, d) / total_words_count(d)) * 100 [%],
 * specific_word_count(t, d) = number of occurrences of word t in document d
 * total_word_count(d) = total number of words of document d
 *
 */
public class Reducer implements Runnable {
    /**
     * Contains the word - count pair for filename 1
     */
    MapperJobDone fileData_1;
    /**
     * Contains the word - count pair for filename 2
     */
    MapperJobDone fileData_2;
    /**
     * Variable containing the similarity percent of the two files
     */
    float result;

    public Reducer(MapperJobDone fileData_1, MapperJobDone fileData_2) {
        this.fileData_1 = fileData_1;
        this.fileData_2 = fileData_2;
    }

    /**
     * The calculation of the similarity will be calculated on a separate thread,
     * this giving the possibility to compute multiple file similarities in parallel
     *
     * It implements the formulas described in class description
     */
    @Override
    public void run() {
        float sum = 0.0f;
        int wordCountFile1 = fileData_1.totalWordCount;
        int wordCountFile2 = fileData_2.totalWordCount;
        float f2;

        /* Iterates through words from the second file hashmap and calculates
           sum(f(t,di) * f(t,dj))
         */
        Iterator wordIterator =  fileData_1.perWordCount.entrySet().iterator();
        while (wordIterator.hasNext()){
            Map.Entry file1pair = (Map.Entry) wordIterator.next();
            f2 = 0;
            float f1 = ((float)((int)file1pair.getValue()) / (float)fileData_1.totalWordCount);
            if (fileData_2.perWordCount.get(file1pair.getKey()) != null) {
                f2 = ((float) ((int) fileData_2.perWordCount.get(file1pair.getKey())) /
                        (float) fileData_2.totalWordCount);
            }
            sum += (f1 * f2 * 100);
        }
        result = sum;
    }

    public MapperJobDone getFileData_1() {
        return fileData_1;
    }

    public MapperJobDone getFileData_2() {
        return fileData_2;
    }

    public float getResult() {
        return result;
    }
}
