package detector.engine;
import utils.containers.Job;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Class used to break the input into multiple chunks and assign those
 * chunks to mappers.
 *
 * Files have o be added so that the splitter could split:)
 */
public class Splitter {
    private List<String>    filenames;
    private int             chunkSize;
    private String          currentFilename;
    private int             currentOffset;
    private long            currentFileLength;

    /**
     * Constructor
     * @param chunkSize Maximum size of chunk to be processed by a single mapper
     */
    public Splitter(int chunkSize) {
        this.filenames = new ArrayList<String>();
        this.chunkSize = chunkSize;
    }

    /**
     * Used to add a new file in the array of filenames
     * @param filename the file to be added
     */
    public void addFile(String filename)
    {
        this.filenames.add(filename);
    }

    /**
     * Used to get a job for a mapper
     * Next job is based on the last file and last offset in the object.
     * If current file ends, current filename is set to null and currentOffset
     * is set to 0;
     * If Current filename is null, a filename is extracted from the list. If no more
     * objects in the list, it returns null.
     *
     * @return Job to be send to a mapper
     */
    public Job getNextJob()
    {
        Job job;

        //TODO: check if file exist on disk
        if (currentFilename == null){
            if (filenames.size() == 0)
                return null;
            currentFilename = filenames.get(0);
            filenames.remove(0);
            currentFileLength = new File(currentFilename).length();
        }

        if (currentFileLength - (chunkSize + currentOffset) > 0) {
            job = new Job(currentFilename, currentOffset, currentOffset + chunkSize);
            currentOffset += chunkSize;
        } else {
            job = new Job(currentFilename, currentOffset, (int)currentFileLength);
            currentFilename = null;
            currentOffset = 0;
        }
        return job;
    }
}
