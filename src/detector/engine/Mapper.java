package detector.engine;

import utils.containers.MapperJobDone;
import utils.logging.Logger;

import java.awt.*;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


/**
 * Class implementing the mapper part of MapReduce paradigm.
 * Implements runnable so it can be applied in a parallel manner on multiple
 * texts.
 *
 * @author Dan Avram
 */
public class Mapper implements Runnable {
    private HashMap<String, Integer> wordFreqPairs;
    private String  file;
    private int     totalWordCount;
    private int     startIndex;
    private int     endIndex;
    private String  data;

    /**
     * Mapper class constructor
     * @param file The file we work on
     * @param startIndex The byte index where we start working
     * @param endIndex The byte index were we stop
     */
    public Mapper(String file, int startIndex, int endIndex) {
        this.file = file;
        this.startIndex = startIndex;
        this.endIndex = endIndex;
        this.wordFreqPairs = new HashMap<>();
    }

    /**
     * Function to execute on separate thread. After setting up the class and
     * calling the new Thread(Mapper) it begins working.
     */
    @Override
    public void run() {
        Logger.log("Working on file %s, between %d and  %d.",
                    file, startIndex, endIndex);

        StringBuilder stringBuilder = new StringBuilder();
        char[] charBuffer = new char[endIndex - startIndex + 2];
        try {
            //TODO: Change filename
            FileReader reader = new FileReader(file);

            /*
            Check if we are inside of a word. If so skip current word.
            To check if we are inside a word, we need to check previous
            character.
             */
            if(startIndex>0)
                startIndex--;

            reader.skip(startIndex);
            reader.read(charBuffer, 0, endIndex - startIndex + 1);

            int indexStartInCharArray = 0;

            /*
            If startIndex is 0 we don't need to check if we are
            inside a word
             */
            if (startIndex != 0) {
                for (int i = startIndex; i < endIndex; i++) {
                    if (Character.isLetterOrDigit(charBuffer[indexStartInCharArray])) {
                        indexStartInCharArray++;
                    } else {
                        break;
                    }
                }
            }
            stringBuilder.append(charBuffer,indexStartInCharArray,endIndex-startIndex-indexStartInCharArray + 1);

            /*
            Check if last char is a character is alphanumeric => we could be inside
             a word so we check if next char is letter or digit and if so append
             it to the string
            Do this until a non-alphanumerical character is found
             */
            char[] nextChar = new char[1];
            reader.read(nextChar, 0,1);
            if (Character.isLetterOrDigit(charBuffer[charBuffer.length - 1])){
                while(Character.isLetterOrDigit(nextChar[0])){
                    stringBuilder.append(nextChar[0]);
                    reader.read(nextChar,0,1);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        data = stringBuilder.toString();
        //TODO: What if '-'
        String delimiters = "\\W+";
        String[] tokens = data.split(delimiters, -1);

        totalWordCount = tokens.length;
        for (String stringIterator : tokens){
            if(stringIterator.equals("")){
                totalWordCount--;
                continue;
            }
            stringIterator = stringIterator.toLowerCase();
            Integer oldValue = wordFreqPairs.get(stringIterator);
            if (oldValue == null) {
                wordFreqPairs.put(stringIterator, 1);
            } else {
                wordFreqPairs.put(stringIterator, oldValue + 1);
            }
        }
    }

    /**
     * Prints pairs of words and number of occurrences in the word-frequency hashmap Hashmap
     */
    public void print() {
        Iterator it = wordFreqPairs.entrySet().iterator();
        while(it.hasNext()){
            Map.Entry pair = (Map.Entry) it.next();
            System.out.println(pair.getKey() + " = " + pair.getValue());
        }
    }

    /**
     * Gets the result of the "mapping" process
     * @return MapperJobDone - structure containing data about finished job
     */
    public MapperJobDone getJobDone() {
        return new MapperJobDone(totalWordCount, file, wordFreqPairs);
    }
}
