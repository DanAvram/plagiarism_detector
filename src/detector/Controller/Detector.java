package detector.Controller;
import detector.engine.Mapper;
import detector.engine.Reducer;
import detector.engine.Splitter;
import utils.containers.Job;
import utils.containers.MapperJobDone;
import utils.logging.Logger;

import java.io.*;
import java.util.*;
import java.util.concurrent.*;

/**
 * Class which implements the actual plagiarism detector.
 * It has to be instantiated in main.
 *
 * @author Dan Avram
 */
public class Detector {
    private HashMap<String, MapperJobDone> fileHashMap;
    private ExecutorService         threadPoolMapper;
    private ExecutorService         threadPoolReducer;
    private List<Future<?>>         futureListMapper;
    private List<Future<?>>         futureListReducer;

    private List<Mapper>            mappersList;
    private List<Reducer>           reducerList;
    private Splitter                splitter;
    private String                  fileToBeChecked;
    private String                  fileOut;
    private int                     chunkSize;
    private float                   similarityThreshold;

    /**
     * Constructor
     * @param threadNumber number of threads in the threadPoolMapper
     * @param fileIn file where are the configurations
     * @param fileOut file to write the results
     */
    public Detector(int threadNumber, String fileIn, String fileOut)
   {
        fileHashMap = new HashMap<>();
        mappersList = new ArrayList<>();
        reducerList = new ArrayList<>();
        this.fileOut = fileOut;

        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(fileIn));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.println("Input file not found!");
            return;
        }
        Logger.log("%s will be used as input file, and the results will be written in %s.\n",
                    fileIn, fileOut);
        Logger.log("%d threads will be used.\n\n", threadNumber);

        try {
            fileToBeChecked     = new String(reader.readLine());
            chunkSize           = Integer.parseInt(reader.readLine());
            similarityThreshold = Float.parseFloat(reader.readLine());
            reader.readLine();

        } catch (IOException e) {
            e.printStackTrace();
        }
        splitter = new Splitter(chunkSize);
        threadPoolMapper = Executors.newFixedThreadPool(threadNumber);
        threadPoolReducer = Executors.newFixedThreadPool(threadNumber);
        futureListMapper = new ArrayList<>();
        futureListReducer = new ArrayList<>();

        String iterator;
        try{
            while ((iterator = reader.readLine()) != null){
                splitter.addFile(iterator);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * The place where magic happens
     * Creates a mapper for every task end executes it
     * on a thread in the thread pool
     */
   public void startIndexing(){
       Job nextJob;
       while((nextJob = splitter.getNextJob()) != null){
           Mapper mapper = new Mapper(nextJob.filename, nextJob.startIndex, nextJob.endIndex);
           mappersList.add(mapper);
           futureListMapper.add(threadPoolMapper.submit(mapper));
       }
       Logger.log("Indexing function exit.");
   }

    /**
     * Blocks until all threads in the thread pool finish
     * Should be called to wait for all the mappers to finish
     * their jobs
     */
    public void waitTerminateMappers() {
        for (Future f: this.futureListMapper){
            try {
                f.get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }
        threadPoolMapper.shutdownNow();
    }

    /**
     * Blocks until all threads in the reducers' thread pool finish
     * Should be called to wait for all the reducers to finish their jobs
     */
    public void waitTerminateReducers() {
       for (Future f: this.futureListReducer){
           try {
               f.get();
           } catch (InterruptedException e) {
               e.printStackTrace();
           } catch (ExecutionException e) {
               e.printStackTrace();
           }
       }
       threadPoolReducer.shutdownNow();
   }

    /**
     * Used after computing word frequencies on text parts.
     *
     * It combine those partial results into a MapperJobDone object
     * Basically it creates a list of pairs of words and their count on a per file basis and stores them
     * as a MapperJobDone object
     * The MapperJobDone objects are stored in a HashMap using as key the filename.
     *
     * Implementation: function iterates through all mapper objects and populates the MapperJobDone Hashmap
     *      - for every mapper the strings and their count are merged into a single MapperJobDone object
     *      - if the mapper's file doesn't already exists, the hashmap from the mapper is introduced
     *      in the Detector's general hashmap to the corresponding filename
     *      - if there is already a file with that name, the function will iterate through all Strings
     *      of the mapper's hashTable, inserting the new words and adding the count to the existing words
     *
     */
   public void combineMapperResults()
   {
        //TODO: Maybe use a thread pool to combine mapper results
        Iterator<Mapper> iter = mappersList.iterator();
        while (iter.hasNext()){
            Mapper mapperIter = iter.next();
            MapperJobDone mapperJobDone = mapperIter.getJobDone();
            iter.remove();

            MapperJobDone detectorJobDone;
            detectorJobDone = fileHashMap.get(mapperJobDone.fileName);

            if (detectorJobDone == null){ //if the file hasn't been introduced yet
                 fileHashMap.put(mapperJobDone.fileName, mapperJobDone);
            } else {
                 HashMap<String, Integer> detectorHashMap = detectorJobDone.perWordCount;
                 Iterator mapperHashMapIterator = mapperJobDone.perWordCount.entrySet().iterator();

                 detectorJobDone.totalWordCount += mapperJobDone.totalWordCount;
                 while(mapperHashMapIterator.hasNext()){
                     Map.Entry pair =(Map.Entry) mapperHashMapIterator.next();
                     Integer oldValue = detectorHashMap.get(pair.getKey());
                     if (oldValue == null) {
                         detectorHashMap.put((String) pair.getKey(), (Integer) pair.getValue());
                     } else {
                         detectorHashMap.put((String) pair.getKey(), (Integer)pair.getValue() + oldValue);
                     }
                 }

            }
        }
   }

    //TODO: Already calculated similarities should be cached
    //public void compute

    /**
     * Calculates similarities(in [%] ) between the fileToBeChecked and the rest of the files indexed earlier
     * The file must have already been indexed and exist in fileHashMap
     *
     * It creates Reducer objects and execute them on separate threads using a thread pool
     * @see Reducer
     * @see Mapper
     *
     * @param fileToBeChecked the file you want to check
     */
    public void checkSimilarities(String fileToBeChecked) {
        Reducer reducer;
        MapperJobDone fileData1 = fileHashMap.get(fileToBeChecked);
        Iterator it = this.fileHashMap.entrySet().iterator();
        while(it.hasNext()){
            Map.Entry pair = (Map.Entry) it.next();

            /* If the file is the one checked, skip it. Uncomment to work */
//            if (((String)pair.getKey()).equals(fileToBeChecked)){
//                 continue;
//           } else { // else process it
                 reducer = new Reducer(fileData1, (MapperJobDone)pair.getValue());
                 reducerList.add(reducer);
                 threadPoolReducer.execute(reducer);
 //           }
        }
    }

    /**
     * Same as checkSimilarities(String), only difference being that it checks the file
     * received in constructor
     */
    public void checkSimilarities()
    {
        checkSimilarities(fileToBeChecked);
    }


    /**
     * Gets data from reducers and write them to the output file
     * @exception FileNotFoundException
     * @see PrintWriter
     * @see Reducer
     */
    public synchronized void writeResultsToFile()
    {
        PrintWriter writer;
        try {
            writer = new PrintWriter(this.fileOut);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return;
        }
        writer.printf("Rezultate pentru: (%s)\n", fileToBeChecked);
        writer.println();

        /* Iterate through reducers */
        Iterator<Reducer> iter = reducerList.iterator();
        while(iter.hasNext()){
            Reducer reducerIter = iter.next();
            if (reducerIter.getResult() >= similarityThreshold) { // if similarities exceeds the threshold, print
                writer.printf("%s (%.3f)\n", reducerIter.getFileData_2().fileName, reducerIter.getResult());
            }
            iter.remove();
        }
        writer.close();
    }


}
