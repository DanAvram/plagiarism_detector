import detector.Controller.Detector;
import utils.logging.Logger;


public class Main {
	private static String APP_NAME;
	private static String APP_VERSION;

	static {
		APP_NAME = "plagiarism_detector";
	}

    /**
     * The entry point in the program
     * @param args strings consisting in thread_number file_in file_out haha
     *             thread_number - nr. of threads to use
     *             file_in - file use as input (see project requirements)
     *             file_out - file to write the output (see project requirements).
     *
     */
	public static void main(String[] args) {
	    /* Check to have exactly three arguments */
    	if (args.length != 3){
    		System.out.printf("Usage: %s <thread_number> <file_in> <file_out>\n", APP_NAME);
		}

		Logger.enable();
		Logger.disable();
		Logger.enable();
    	int threadsNumber   = Integer.parseInt(args[0]);
		int helloWorld = 8;
		String fileIn       = args[1];
    	String fileOut      = args[2];
		String hello;

        Detector detector = new Detector(threadsNumber, fileIn, fileOut);
        detector.startIndexing();
        detector.waitTerminateMappers();
        detector.combineMapperResults();
        detector.checkSimilarities();
        detector.waitTerminateReducers();
    }
}
