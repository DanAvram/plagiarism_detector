package utils.logging;

/**
 * Logger is a "static" class used to log different useful
 * information about the application flow. It should not be instantiated!
 *
 * It can be enabled or disabled using the "enabled" member variable.
 * @author Dan Avram
 */
public class Logger {
    static boolean enabled = false;
    protected Logger(){}

    /**
     * Check if the logger is enabled.
     * @return Logger state
     */
    public static boolean isEnabled(){
        return enabled;
    }

    /**
     * Enable logger globally
     */
    public static void enable() {
        Logger.enabled = true;
    }

    /**
     * Disable logger globally
      */
    public static void disable() {
        Logger.enabled = false;
    }

    /**
     * Log information
     * @param printfString string as in printf
     * @param args arguments for previous printf format string
     */
    public static synchronized void log(String printfString, Object ... args ){
        if (Logger.isEnabled()){
            System.out.printf(printfString, args);
            System.out.println();
        }
    }
}
