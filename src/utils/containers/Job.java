package utils.containers;

/**
 * Class used to encapsulate data about a task to be send to a mapper
 * Contains data about filename, start offset and end offset
 * @author Dan Avram
 */
public class Job {
    public String filename;
    public int startIndex;
    public int endIndex;

    public Job(String filename, int startIndex, int endIndex) {
        this.filename = filename;
        this.startIndex = startIndex;
        this.endIndex = endIndex;
    }
}
