package utils.containers;

import java.util.HashMap;

/**
 * Class containing the result of a done job executed by a mapper.
 * @author Dan Avram
 */
public class MapperJobDone {
    /**
     * Number of words in the file
     */
    public int                      totalWordCount;

    /**
     * The name of the file
     */
    public String                   fileName;

    /**
     * Contains pairs of words and their count in the file
     */
    public HashMap<String, Integer> perWordCount;

    public MapperJobDone(){}
    public MapperJobDone(int totalWordCount, String fileName, HashMap<String, Integer> perWordCount) {
        this.totalWordCount = totalWordCount;
        this.fileName = fileName;
        this.perWordCount = perWordCount;
    }

}
