# Document indexing using Map reduce paradigm

This project aims to create a tool to easily index different document with the end purpose of checking similarities
between files.

Project created using java and IntelliJ as part of a homework for Distributed Systems lab.

For more details about implementation please check [RO]Requirements_and_Implementation_details.docx which describe in
more detail the project.

# How it works [short]

## Class Detector
Class Detector encapsulates all the project's functionality. It contains a list of mappers which work on word counting
in files, a list of reducers which combines the results of the mappers on a per-file basis. Both of this lists are 
created to keep trace of the mappers and reducers.

It also contains two list of Features used to wait for mappers and reducers threads to finish.

The detectors has a splitter to create tasks(jobs) for mappers - <b>Job</b>

<b>fileHashMap</b> - it's a hashmap indexed by file name and containing the result of the mappers.

## Class Mapper
Counts the words in a file between specified indexes. It implements <b>Runnable</b> interface so it should be run on a 
new thread.

## Class Reducer
Calculates the results of similarity check for each and every file on a separate thread for each file.

## Class Splitter
Splits a file into jobs for mappers. 

## Class Job
Contains data about a job - created by <b>Splitter</b> and used by <b>Mapper</b>

## Class MapperJobDone
Contains data of a finished <b>Mapper</b>

## Logger
Logs data. Useful for debugging or printing information about the program execution. Static class. Should not be 
instantiated. Use only static functions.

# Work flow
### Code:
```
1      Detector detector = new Detector(threadsNumber, fileIn, fileOut);
2      detector.startIndexing();
3      detector.waitTerminateMappers();
4      detector.combineMapperResults();
5      detector.checkSimilarities();
6      detector.waitTerminateReducers();
7      detector.writeResultsToFile();
```

### Explanation:
1) Create detector
2) Start indexing. Create splitter and start mappers.
3) Wait for mappers to end their job so we can start processing.
4) Combine mapper fragments into "per-file" statistics.
5) Checking similarities. Create reducers and apply the formula.
6) Wait reducers threads to finish.
7) Write the application results into file.